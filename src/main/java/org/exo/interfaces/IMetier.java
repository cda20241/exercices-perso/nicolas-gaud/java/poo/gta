package org.exo.interfaces;

import org.exo.models.Personnage;
import org.exo.models.Vehicule;

public interface IMetier {


    public int listerCompetences();
    public void effectuerTravail(int choix, Vehicule vehicule, Personnage personnage);
    public void facturerClient(Personnage personnage, int tarif);

}

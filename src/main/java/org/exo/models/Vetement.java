package org.exo.models;

import org.exo.enums.Chaussure;
import org.exo.enums.Taille;
import org.exo.enums.Type;

public class Vetement {
    private Object typeVetement;
    private String couleurVetement;
    private Object tailleVetement;

    public Object getTaille() {
        return tailleVetement;
    }

    public Vetement(Type type, String couleur, Taille taille) {
        typeVetement = type;
        couleurVetement = couleur;
        tailleVetement = taille;
    }

    public Vetement(Chaussure chaussures){
        typeVetement = chaussures.getType();
        tailleVetement = chaussures.getTaille();
    }

    public String getCouleurVetement() {
        return couleurVetement;
    }

    public void setCouleurVetement(String couleur) {
        this.couleurVetement = couleur;
    }
}

package org.exo.models;

public class Moteur {

    private Integer pointsDeVie;

    public Moteur(Integer pointsDeVie) {
        this.pointsDeVie = pointsDeVie;
    }

    public Integer getPointsDeVie() {
        return pointsDeVie;
    }

    public void setPointsDeVie(Integer pointsDeVie) {
        this.pointsDeVie = pointsDeVie;
    }

}

package org.exo;

import org.exo.enums.Chaussure;
import org.exo.enums.Taille;
import org.exo.enums.Type;
import org.exo.models.*;
import org.exo.utils.Util;

import java.util.List;

public class Main {
    public static void main (String[] args) {

        //Lecture des fichiers de sauvegarde et recupération dans des listes
        List<Personnage> personnages = Util.readJsonFileAndRenderPerso();
        List<Vehicule> vehicules = Util.readJsonFileAndRenderVehicule();

        //on supprime les fichiers de sauvegarde
        Util.deleteJsonFiles();

        //on instancie les personnages nécessaires au sénario//

        Personnage michel = new Personnage("Michel", "M", 183, 9);

        //on habille notre personage//

        michel.addVetement(new Vetement(Type.jean, "bleu", Taille.L));
        michel.addVetement(new Vetement(Type.tshirt, "blanc", Taille.L));
        michel.addVetement(new Vetement(Chaussure.Talons));
        michel.addVetement(new Vetement(Type.pull, "vert", Taille.XL));
        michel.setTailleTotale();

        Garagiste garagiste = new Garagiste("Bébert", "M", 165, 4);
        garagiste.addVetement(new Vetement(Type.jean, "bleu", Taille.L));
        garagiste.addVetement(new Vetement(Type.tshirt, "blanc", Taille.L));
        garagiste.addVetement(new Vetement(Chaussure.Talons));
        garagiste.addVetement(new Vetement(Type.pull, "vert", Taille.XL));

        Coiffeur coiffeur = new Coiffeur("Tatiana", "F", 173, 60);

        //on instancie des véhicules//

        Voiture voiture1 = new Voiture();
        voiture1.setModele("Carrera 3");
        Moto moto1 = new Moto();
        moto1.setModele("VMax");

        System.out.println("\n### CHEZ LE COIFFEUR ###\n");

//        int choix = coiffeur.listerCompetences();
//        coiffeur.effectuerTravail(choix, null, michel);

        System.out.println("\n### ACHAT VOITURE ###");

        voiture1.acheterVehicule(michel);

        System.out.println("\n### INTERRACTIONS AVEC VEHICULES ###");

        //conduit la voiture et la met dans l'eau//

        voiture1.conduireVehicule(michel);
        voiture1.sousleau();
        voiture1.sortirDuVehicule(michel);

        //il conduit une moto et le garagiste tente de la conduire//

        moto1.conduireVehicule(michel);
        moto1.conduireVehicule(garagiste);

        //conduit la moto et multiples collisions//

        moto1.conduireVehicule(michel);
        int nombreCollisions = 2;

        for (int i = 0; i < nombreCollisions; i++){
            moto1.collision(Moto.NB_POINT_COLL);
        }

        System.out.println("\n### LE GARAGISTE ###");

        //fait réparer et peindre la carosserie//

        garagiste.reparerCarrosserie(moto1, michel);

        moto1.conduireVehicule(garagiste);

        //tente de conduire la voiture//

        //garagiste.repeindreVehicule(moto1, michel);

        //le garagiste répare le moteur de la voiture//

        garagiste.reparerMoteur(voiture1, michel);
        garagiste.addVetement(new Vetement(Type.tshirt, "rouge", Taille.XL));

        Util.serializePersonToFile(michel);
        Util.serializePersonToFile(garagiste);
        Util.serializeVehiculeToFile(voiture1);
        Util.serializeVehiculeToFile(moto1);
    }
}

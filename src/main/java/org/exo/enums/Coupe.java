package org.exo.enums;

public enum Coupe {
    zidane("Zidane", 0),
    militaire("Militaire", 3),
    courts("Courts",9),
    miLongs("Mi-Longs", 22),
    longs("Longs", 40);
    private String label;
    private int value;

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    private Coupe (String label, int value){
        this.label = label;
        this.value = value;
    }

}



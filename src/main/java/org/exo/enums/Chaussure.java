package org.exo.enums;

public enum Chaussure {
    Basket ("Basket",3),
    Talons ("Talons",10);
    private String type;
    private Integer taille;

    Chaussure(String type, Integer taille) {
        this.type = type;
        this.taille = taille;
    }

    public String getType() {
        return type;
    }

    public Integer getTaille() {
        return taille;
    }


}
